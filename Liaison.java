import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class Liaison{
    public static Connection liaisonOracle(){
        String url = "jdbc:oracle:thin:@localhost:1521:orcl"; 
        String user = "article"; 
        String password = "0000";

        Connection connection = null;
        try {
            Class.forName("oracle.jdbc.driver.OracleDriver");
            connection = DriverManager.getConnection(url, user, password);
        } catch (ClassNotFoundException e) {
            System.out.println("Erreur de chargement du pilote JDBC Oracle : " + e.getMessage());
        } catch (SQLException e) {
            System.out.println("Erreur de connexion à la base de données : " + e.getMessage());
        }
        return connection;
    }
}