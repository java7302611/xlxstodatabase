import java.sql.Statement;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.lang.reflect.Field;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;

public class Traitment {
    public void insert(Connection c,String value) throws SQLException{
        if(c==null){
            c = Liaison.liaisonOracle();
        }

        String sql = "insert into "+this.getClass().getSimpleName()+" (";
        Field[] liste_field = this.getClass().getDeclaredFields();
        for (int i = 0; i < liste_field.length; i++) {
            sql = sql + liste_field[i].getName();
            if(i!=liste_field.length-1){
                sql = sql + ",";
            }
        }
        sql = sql + ") VALUES "+value;
        System.out.println(sql);
        try {

            PreparedStatement preparedStatement = c.prepareStatement(sql);
           
            preparedStatement.executeUpdate();

           preparedStatement.close();
           c.commit();

        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

    public void a (Connection c,String sql) throws SQLException{
        try {

            Statement stmt = c.createStatement();;
           
            stmt.executeUpdate("insert into Unite (identifiant,designation) VALUES ('UNI01','AMPOULE')");
            stmt.close();

           c.commit();

        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        c.close();
        
    }
    public  void insertDataCsvToDatabase(Connection c,String path) throws IOException, SQLException{
        BufferedReader bf = new BufferedReader(new FileReader(path));
        String ligne = "";
        int i=0;
        String value="";
        Field[] liste_field = this.getClass().getDeclaredFields();
        while((ligne = bf.readLine()) != null){
            if(i!=0){
                String[] colonne = ligne.split(",");
                value = "(";
                for (int j = 0; j < colonne.length; j++) {
                    String type = liste_field[j].getType().getSimpleName();
                    if(type.equals("String")){
                        value = value+"'"+colonne[j]+"'";
                    }else if(type.equals("double") || type.equals("int")){
                        value = value+colonne[j];
                    }
                    if(j!=(colonne.length-1)){
                        value = value + ",";
                    }
                }
                value = value+")";
                this.insert(c, value);
            }
            i++;
        }
    }
}
